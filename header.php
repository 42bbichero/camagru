<!DOCTYPE html>
<html lang="en" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport"  content="width=device-width" />
	<link rel="stylesheet" href="<?php echo $url_path;?>/css/header.css">
	<link rel="stylesheet" href="<?php echo $url_path;?>/css/like.css">
	<link rel="stylesheet" href="<?php echo $url_path;?>/css/comment.css">
	<title>Camagru</title>
</head>
<body>
	<header>
		<div class="title">
			<span class="letter" data-letter="C">C</span>
			<span class="letter" data-letter="A">A</span>
			<span class="letter" data-letter="M">M</span>
			<span class="letter" data-letter="A">A</span>
			<span class="letter" data-letter="G">G</span>
			<span class="letter" data-letter="R">R</span>
			<span class="letter" data-letter="U">U</span>
		</div>
	</header>
