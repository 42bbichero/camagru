<?PHP
require("config/connect.php");
if (!isset($_SESSION['auth']['username']) || $_SESSION['auth']['active'] !== 'Y')
	header("Location:$url_path/auth.php");
require("header.php");
require("comment/Comment.class.php");
?>
<center><div><?PHP echo  'Logged as ' . $_SESSION['auth']['username']?></div></center>
<center><div style="display:inline-block;width:60%">
	<button onclick="document.location.href='<?PHP echo $url_path; ?>/camagru.php'"     style="float:left;"  class="button_c">Home</button>
	<button onclick="document.location.href='<?PHP echo $url_path; ?>/auth.php?logout'" style="float:right;" class="button_c">Logout</button>
</div></center>
<div class="wall" style="display:block;margin-left:auto;margin-right:auto;" align="center">
  <script type="text/javascript" src="js/comment.js"></script>
  <?PHP
  $max_img = 5;
  $nb_img = $PDO->query('SELECT COUNT(*) AS nb_img FROM web_pictures')->fetchColumn();
  $nb_page  = ceil($nb_img / $max_img);
  echo 'Page : ';
  $cpt = 1;
  while ($cpt <= $nb_page)
  {
    echo "<a href='$url_path/gallery.php?page=" . $cpt . "'>" . $cpt . "</a> ";
    $cpt++;
  }
  if (isset($_GET['page']))
    $page = $_GET['page'];
  else
    $page = 1;
  $fst_img = ($page -1) * $max_img;
  $req = $PDO->query('SELECT * FROM web_pictures ORDER BY id DESC LIMIT ' . $fst_img . ', ' . $max_img);
  while ($img = $req->fetch(PDO::FETCH_ASSOC))
  {
    echo "<br>";
    $comments = array();
    $result = $PDO->query(
    "SELECT comments.id, comments.body, comments.creation_date
    FROM comments
    WHERE id_web_picture = " . $img['id'] . "
    ORDER BY id ASC");

    // This is comment preparation for user display.
    while($row = $result->fetch(PDO::FETCH_ASSOC))
      $comments[] = new Comment($row);
    echo '<img  class="displayed" src="data:image/png;base64,' . $img['img_encode'] . '" />';?>
    <br><span   id="status"></span><br>
    <button class="button_like"    id="like_<?PHP echo $img['id'];?>"   onClick="ft_like('<?PHP echo $img['id'];?>')"    value="<?PHP echo $img['nb_like'];?>">
      <span id="<?PHP echo $img['id'];?>l"><?php echo $img['nb_like'];?></span>
    </button>
    <button class="button_unlike"  id="unlike_<?PHP echo $img['id'];?>" onClick="ft_unlike('<?PHP echo $img['id'];?>')"  value="<?PHP echo $img['nb_unlike'];?>">
      <span id="<?PHP echo $img['id'];?>"><?PHP echo $img['nb_unlike'];?></SPAN>
    </button>
    <script type="text/javascript" src="<?PHP echo $url_path; ?>/js/like.js"></script>
    <br><br><button   class="button_trash"   id="trash"  onclick="del_picture('<?PHP echo $img['id']?>', '<?PHP echo $img['id_user']?>')"></button><br><br>
    <!--This is the commentary section, with the rendering and the sealing-date from the body filled.-->
    <div id="com<?php echo $img['id'];?>">
      <?php
        foreach($comments as $c)
          echo $c->date();
        ?>
    </div>
    <div        id="addCommentContainer">
      <button   id="comment" class="button_b" onClick="add_comment('<?PHP echo $img['id']; ?>', '<?PHP echo date('d M Y H:i');?>')">Comment</button>
      <textarea id="body<?PHP echo $img['id'];?>" name="body" maxlenght="600"></textarea><br>
    </div>
    <?PHP
    }?>
  <img id="photo">
</div>
<br>
<!--Here are scripts includes needed for the good use of the javascpript utilisation.-->
<script type="text/javascript" src="<?PHP echo $url_path; ?>/js/del_picture.js"></script>
</body>
