<?php
require ("config/connect.php");

if (isset($_POST["ResetPasswordForm"]))
{
  // Get the users values and check the security of the password.
  $email = $_POST["email"];
  $password = $_POST["password"];
  $confirmpassword = $_POST["confirmpassword"];
  $hash = $_POST["q"];
  if (!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/", $password))
  {
    header("Location: $url_path/auth.php?reset_2w=$hash");
    exit();
  }
  $salt = "498#2D83B631%3800EBD!801600D*7E3CC13";
  $resetkey = hash('sha512', $salt.$email);

  // Check if the secure key matchs to the key sent by mail to the user.
  if ($resetkey == $hash)
  {
    if ($password == $confirmpassword)
    {
      $password = hash('sha512', $password);

      // If the password is regular, send a success request and update the password in the database.
      $req = $PDO->prepare('UPDATE users SET password = :password WHERE email = :email', array('password' => $password, 'email' => $email));
      $PDO = null;
      echo '<div style="
        text-align: center;
        margin: 20px auto;
      	width: 343px;
      	max-height:80%;
      	padding:4px;
      	font-size: 15px;
      	text-align: center;
      	-webkit-border-radius: 8px/7px;
      	-moz-border-radius: 8px/7px;
      	border-radius: 8px/7px;
      	background-color: #ebebeb;
      	-webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
      	-moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
      	box-shadow: 1px 2px 5px rgba(0,0,0,.31);
      	border: solid 1px #cbc9c9;">Your password has been successfully reset.</div>';
    }
    // Print an error if passwords are different/
    else
      echo '<div style="
        text-align: center;
        margin: 20px auto;
      	width: 343px;
      	max-height:80%;
      	padding:4px;
      	font-size: 15px;
      	text-align: center;
      	-webkit-border-radius: 8px/7px;
      	-moz-border-radius: 8px/7px;
      	border-radius: 8px/7px;
      	background-color: #ebebeb;
      	-webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
      	-moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
      	box-shadow: 1px 2px 5px rgba(0,0,0,.31);
      	border: solid 1px #cbc9c9;">Your password do not match.</div>';
  }
  // Print an error if the two security keys are different.
  else
    echo '<div style="
      text-align: center;
      margin: 20px auto;
    	width: 343px;
    	max-height:80%;
    	padding:4px;
    	font-size: 15px;
    	text-align: center;
    	-webkit-border-radius: 8px/7px;
    	-moz-border-radius: 8px/7px;
    	border-radius: 8px/7px;
    	background-color: #ebebeb;
    	-webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
    	-moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
    	box-shadow: 1px 2px 5px rgba(0,0,0,.31);
    	border: solid 1px #cbc9c9;">Your password reset key is invalid.</div>';
}?>
