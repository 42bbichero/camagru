<?php

//disabled display of SID
// ini_set('session.use_trans_sid', false);

//Initiate the session
if(!isset($_SESSION))
  session_start();

//Set a default timestamp
date_default_timezone_set('Europe/Paris');

// show all error
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Include personal conf for DB and site
$url_path = "/camagru";

//Option for log request : SW -> log request SHOW, SELECT and INSERT INTO, UPDATE  RW-> Only INSERT INTO, UPDATE
$log_request = "RW";

//Show all request 1 -> show all  0 -> show nothing
$debug_req = 0;

// DB conf
$DB_DSN = "mysql:dbname=camagru;host=localhost";
$DB_USER = "root";
$DB_PASSWORD = "root";
$option = array(
    // Activation PDO exceptions:
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_TIMEOUT => 10
  );

//Include Connection class
require_once("Connection.class.php");
?>
