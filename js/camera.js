(function()
{
	var streaming 	 = false,
	video        		 = document.querySelector('#video'),
	cover        		 = document.querySelector('#cover'),
	canvas        	 = document.querySelector('#canvas'),
	photo         	 = document.querySelector('#photo'),
	startbutton   	 = document.querySelector('#startbutton'),
	width				  	 = 500,
	height			  	 = 0;

	// // // // // // // // // // // // // // // // // // // // // // // // // // //
	//                           BROWSERS COMPATIBILITY:
	// This feature allows operations to work on every browsers, isn't it awesome ?
	// // // // // // // // // // // // // // // // // // // // // // // // // // //

	if (navigator.mediaDevices === undefined)
	{
		navigator.mediaDevices = {};
	}

	if (navigator.mediaDevices.getUserMedia === undefined)
	{
		navigator.mediaDevices.getUserMedia = function(constraints)
		{
			var getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia);
			if (!getUserMedia)
			{
				return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
			}
			return new Promise(function(resolve, reject)
			{
				getUserMedia.call(navigator, constraints, resolve, reject);
			});
		}
	}

	navigator.mediaDevices.getUserMedia({ audio: false, video: true })
	.then(function(stream)
	{
		var video = document.querySelector('video');
		// Older browsers may not have srcObject
		video.src = window.URL.createObjectURL(stream);
		video.onloadedmetadata = function(e)
		{
			video.play();
		};
	})
	.catch(function(err) {
		console.log(err.name + ": " + err.message);
	});

	video.addEventListener('canplay', function(ev)
	{
		if (!streaming)
		{
			height = video.videoHeight / (video.videoWidth/width);
			video.setAttribute('width', width);
			video.setAttribute('height', height);
			canvas.setAttribute('width', width);
			canvas.setAttribute('height', height);
			streaming = true;
		}
	}
	, false);

	// // // // // // // // // // // // // // // // // // // // // // // // // // //
	//                              TAKE A PICTURE:
	// This part is simply used to take a photo of the video flux displayed on the
	// screen.
	// The last two functions serve to start the taking of the photo. One by using
	// the keyboard, the other with a clickable button.
	// // // // // // // // // // // // // // // // // // // // // // // // // // //

	function sleep(milliseconds)
	{
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++)
		{
			if ((new Date().getTime() - start) > milliseconds)
			break;
		}
	}

	function takepicture()
	{
		canvas.width = width;
		canvas.height = height;
		canvas.getContext('2d').drawImage(video, 0, 0, width, height);
		var data = canvas.toDataURL('image/png');
		photo.setAttribute('src', data);

		document.getElementById('hidden_data').value = data;
		var fd = new FormData(document.forms["form1"]);
		var xhr = new XMLHttpRequest();
		xhr.open('POST', 'img_post.php', true);
		xhr.upload.onprogress = function(e)
		{
			if (e.lengthComputable)
			var percentComplete = (e.loaded / e.total) * 100;
		};
		xhr.onload = function() {};
		xhr.send(fd);
	}

	function reload()
	{
		sleep(100);
		location.reload(true);
	}

	// Trigger //
	startbutton.addEventListener('click', function(ev)
	{
		if (streaming == true)
		{
			takepicture();
			reload();
			ev.preventDefault();
		}
	}
	, false);
	// Trigger off //
}
)();
