<?php
require("config/connect.php");

// Check if a POST is set
if (isset($_GET['id']))
{
	if (isset($_GET['like']))
	{
		if ($PDO->prepare("UPDATE web_pictures SET nb_like = :nb_like WHERE id = :id",
		array("nb_like" => $_GET['nb_like'] + 1, "id" => $_GET['id'])))
			echo (int)$_GET['nb_like'] + 1;
		else
			echo "ERR";
	}
	else if (isset($_GET['unlike']))
	{
		if ($PDO->prepare("UPDATE web_pictures SET nb_unlike = :nb_unlike WHERE id = :id",
				array("nb_unlike" => $_GET['nb_unlike'] + 1, "id" => $_GET['id'])))
			echo $_GET['nb_unlike'] + 1;
		else
			echo "ERR";
	}
}
else
	echo "no ID";
?>
