function del_picture(id, id_user)
{
  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'del_picture.php?id='+ id + '&id_user=' + id_user);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
  xhr.onload = function()
  {
    if (xhr.responseText != 1)
      alert("You're not able to delete this post !");
    else
      location.reload();
  };
  xhr.send();
}
