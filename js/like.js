function ft_like(id)
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'like.php?id='+ id + '&nb_like=' + document.getElementById('like_' + id).value + '&like=1');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
	xhr.onload = function()
	{
		if (xhr.status === 200)
		{
			document.getElementById(id + 'l').innerHTML = xhr.responseText;
			document.getElementById('like_' + id).value = xhr.responseText;
		}
	};
	xhr.send();
}

function ft_unlike(id)
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'like.php?id='+ id + '&nb_unlike=' + document.getElementById('unlike_' + id).value + '&unlike=1');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
	xhr.onload = function()
	{
		if (xhr.status === 200)
		{
			document.getElementById(id).innerHTML = xhr.responseText;
			document.getElementById('unlike_' + id).value = xhr.responseText;
		}
	};
	xhr.send();
}
