<?PHP
require("config/connect.php");
if ((!isset($_SESSION['auth']['username']) || (!isset($_SESSION['auth']['active']))) || (isset($_SESSION['auth']['active']) && $_SESSION['auth']['active'] === 'N'))
	header("Location:$url_path/auth.php");
require_once("header.php");
require("comment/Comment.class.php");
?>
	<script type="text/javascript" src="<?PHP echo $url_path; ?>/js/mobile.js"></script>
	<link rel="alternate" media="only screen and (max-width: 320px)" href="<?PHP echo $url_path; ?>/camagru_mobile.php">
	<center><div><?PHP echo  'Logged as ' . $_SESSION['auth']['username']?></div></center>
		<div style="display:inline-block;width:100%">
			<button onclick="document.location.href='<?PHP echo $url_path; ?>/gallery.php'"     style="float:left;"  class="button_c">Gallery</button>
			<button onclick="document.location.href='<?PHP echo $url_path; ?>/auth.php?logout'" style="float:right;" class="button_c">Logout</button>
		</div>
		<!--This is the main block of the page with the video flux.-->
		<br><div class="main" style="display:inline-block;position:relative"><br><br>
			<!--This is the upload feature for the user.-->
			<div style="display:block;margin-left:auto;margin-right:auto" align="center">
				<form    method="post"        action="img_post.php" enctype="multipart/form-data">
					<input name="upload"        type="file"           id="find_file"/>
					<input name="hidden_filter" type="hidden"     		id='hidden_filter2'/><br>
					<input name="submit"        type="submit"					id="upload" value="Start Upload"/>
				</form>
			</div>
			<!--This is the video return for the user from camera flux.-->
			<button    id="startbutton">Start</button>
			<video     id="video" class="video"></video>
			<canvas    id="live"            style="max-width:100%;max-height:100%;" class="" ></canvas><br>
				<form    name="form1"         method="post"      action="#"           accept-charset="utf-8" >
					<input name="hidden_filter" id='hidden_filter' type="hidden"/>
					<input name="hidden_data"   id='hidden_data'   type="hidden"/>
				</form>
				<canvas id="canvas"></canvas>
			</div>
			<!--This is the gallery block where pictures taken by users are stocked and where the social features are placed. (like, comment, delete)-->
			<div class="gallery" style="white-space:nowrap;" >
				<!-- <img alt="" src="" id="photo"> -->
				<div id="photo"></div>
				<?PHP
				$req = $PDO->query("SELECT * FROM web_pictures WHERE id_user = ".$_SESSION['auth']['id']." ORDER BY id DESC ");
				$cpt = 0;
				while ($img = $req->fetch(PDO::FETCH_ASSOC))
			  {
					echo '    '.'<IMG alt="img" class="mini" src="data:image/png;base64,' . $img['img_encode'] . '"/>';
					$cpt++;
					if($cpt == 2)
					{
						echo '<BR>';
						$cpt = 0;
					}
				}
				?>
			</div>
			<!--This is the filter part where you can choose a filter to put on the image source, from the camera flux or even the uploaded file.-->
			<div class="filter">
				<script src="<?PHP echo $url_path; ?>/js/camera.js"></script>
				<?PHP /* Display all filters in a div. */
					$dirname = "resources/filter/";
					$images = glob($dirname."*.png");
					foreach($images as $image)
					{?>
						<span id="<?PHP echo $image;?>"  onClick="ft_filter('<?PHP echo $image;?>')"><IMG alt="filter" style="max-width:30%" src="<?PHP echo $image;?>"/></span> <?PHP
					}?>
			</div>
			<!--This is the footer part whith some bulshit text.-->
			<div class="footer"><br>UrMoM Company -  © 2016 </div>
	</body>
</html>

<script>
var hidden = 0;
function action()
{
	if(hidden == 0)
	{
		document.getElementById('upload').style.visibility = 'hidden';
		document.getElementById('startbutton').style.visibility = 'hidden';
		hidden = 1;
	}
	else
	{
		document.getElementById('upload').style.visibility = 'visible';
		document.getElementById('startbutton').style.visibility = 'visible';
	}
}

function ft_filter(img_name)
{
	var img = document.getElementById("resources/filter/1-lol.png");
	img.className = "";
	var img = document.getElementById("resources/filter/2-pepe.png");
	img.className = "";
	var img = document.getElementById("resources/filter/dodge.png");
	img.className = "";
	var img = document.getElementById("resources/filter/kanye.png");
	img.className = "";
	var img = document.getElementById("resources/filter/kim.png");
	img.className = "";
	var img = document.getElementById("resources/filter/spartan.png");
	img.className = "";
	action();

	var live = document.getElementById("live");
	if (img_name == "resources/filter/1-lol.png")
		live.className = "lol";
	else if (img_name == "resources/filter/2-pepe.png")
		live.className = "pepe";
	else if (img_name == "resources/filter/dodge.png")
		live.className = "dodge";
	else if (img_name == "resources/filter/kanye.png")
		live.className = "kanye";
	else if (img_name == "resources/filter/kim.png")
		live.className = "kim";
	else if (img_name == "resources/filter/spartan.png")
		live.className = "spartan";

	var img  = document.getElementById(img_name);
	img.classList.add("filter_click");

	document.getElementById("hidden_filter").value  = img_name;
	document.getElementById("hidden_filter2").value = img_name;
}
action();

</script>
