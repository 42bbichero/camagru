<?PHP
require_once("config/connect.php");
require_once("header.php");
?>
<link rel="stylesheet" type="text/css" href="<?PHP echo $url_path; ?>/css/auth.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<?PHP ob_start();?>
<!--  Here are the 'signup' version of the page auth.php -->
<div class="signup">
	<h1>Registration</h1>
	<form name="signup" method="POST" action="<?PHP echo $url_path;?>/index.php">
		<hr>
	<label id="icon" for="name"><i class="icon-envelope "></i></label>
	<input type="text" name="email" id="email" placeholder="Email" required/>
	<label id="icon" for="name"><i class="icon-user"></i></label>
	<input type="text" name="username" id="name" placeholder="Username" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="password" placeholder="Password" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="retry_password" id="name" placeholder="Confirm Password" required/>
	<br><br>
	<button name="signup" style="float:left"  class="button" onclick="document.location.href='<?PHP echo $url_path; ?>/camagru.php'" type="submit" >Register</BUTTON>
	<button name="signin" style="float:right" class="button" onclick="document.location.href='<?PHP echo $url_path; ?>/auth.php?signin'">Login</BUTTON>
</form>
</div>
<?PHP
	$signup = ob_get_clean();
	ob_start();
?>
<!--  Here are the 'signup wrong' version of the page auth.php -->
<div class="signup">
	<h1>Registration</h1>
	<form name="wrong" method="POST" action="<?PHP echo $url_path;?>/index.php">
		<hr>
	<label id="icon" for="name"><i class="icon-envelope "></i></label>
	<input type="text" name="email" id="email" placeholder="Email" required/>
	<label id="icon" for="name"><i class="icon-user"></i></label>
	<input type="text" name="username" id="name" placeholder="Username" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="password" placeholder="Password" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="retry_password" id="name" placeholder="Confirm Password" required/>
	<br><br>
	<button name="signup" style="float:left"  class="button" onclick="document.location.href='<?PHP echo $url_path; ?>/camagru.php'" type="submit" >Register</button>
	<button name="signin" style="float:right" class="button" onclick="document.location.href='<?PHP echo $url_path; ?>/auth.php?signin'">Login</button>
</form>
</div>
<div class="wrong"> Notice that your email must be valid and your password must be containt at least 8 characters, letters MIN/MAJ and numbers !</div>
<?PHP
	$wrong = ob_get_clean();
	ob_start();
?>
<!--  Here are the 'signin' version of the page auth.php -->
<div class="signin">
	<h1>Login</h1>
	<form name="signin" method="POST" action="<?PHP echo $url_path;?>/index.php">
		<hr>
	<label id="icon" for="name"><i class="icon-user"></i></label>
	<input id="name" type="text"   name="username" placeholder="Username" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="password" placeholder="Password" required/>
	<br><br>
	<button name="signin"  class="button" style="float:right" onclick="document.location.href='<?PHP echo $url_path; ?>/camagru.php'" type="submit">Login</BUTTON>
</form>
	<button name="reset_1" class="button" style="float:left;margin-left:0.9cm;" onclick="document.location.href='<?PHP echo $url_path; ?>/auth.php?reset_1'">Reset</BUTTON>
</div>
<?PHP
	$signin = ob_get_clean();
	ob_start();
?>
<!--  Here are the 'reset_1' version of the page auth.php -->
<div class="reset_1">
	<h1>Reset</h1>
	<form name="reset_1" method="POST" action="<?PHP echo $url_path; ?>/reset_pass.php">
		<hr>
	<label id="icon" for="name"><i class="icon-envelope "></i></label>
	<input type="text" name="email" id="email" placeholder="Email" required/>
	<br><br>
	<input 	style="display:block;margin-left:auto;margin-right:auto;" align="center" class="button_b" type="submit" name="ForgotPassword" value="Reset" />
</form>
</div>
<?PHP
	$reset_1 = ob_get_clean();
	ob_start();
?>
<!--  Here are the 'reset_2' version of the page auth.php -->
<div class="reset_2">
	<h1>Reset</h1>
	<form  name="reset_2" method="POST" action="<?PHP echo $url_path; ?>/reset_key.php">
		<hr>
	<label id="icon" for="name"><i class="icon-envelope "></i></label>
	<input type="text" name="email" id="email" placeholder="Email" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="password" placeholder="Password" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="confirmpassword" id="name" placeholder="Confirm Password" required/>
	<input type="hidden" name="q"
	<?PHP
	if (isset($_GET['reset_2']))
	  echo " value='" . $_GET['reset_2'] ."'";
	else
		echo " value=''";
	echo ' /><input type="submit" style="display:block;margin-left:auto;margin-right:auto;" align="center" class="button_b" name="ResetPasswordForm" value="Reset"'; ?>
 	/>
</form>
</div>
<?PHP
	$reset_2 = ob_get_clean();
	ob_start();
?>
<!--  Here are the 'reset_2 wrong' version of the page auth.php -->
<div class="reset_2">
	<h1>Reset</h1>
	<form  name="reset_2w" method="POST" action="<?PHP echo $url_path; ?>/reset_key.php">
		<hr>
	<label id="icon" for="name"><i class="icon-envelope "></i></label>
	<input type="text" name="email" id="email" placeholder="Email" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="password" placeholder="Password" required/>
	<label id="icon" for="name"><i class="icon-shield"></i></label>
	<input type="password" name="confirmpassword" id="name" placeholder="Confirm Password" required/>
	<input type="hidden" name="q"
	<?PHP
	if (isset($_GET['reset_2w']))
	  echo " value='" . $_GET['reset_2w'] ."'";
	else
		echo " value=''";
	echo ' /><input type="submit" style="display:block;margin-left:auto;margin-right:auto;" align="center" class="button_b" name="ResetPasswordForm" value="Reset"'; ?>
 	/>
	</form>
</div>
<div class="wrong"> Notice that password must be containt at least 8 characters, letters MIN/MAJ and numbers !</div>
<!-- It is the selector who choose a version page created up there. -->
<?PHP
$mark = 0;
if ($_GET['username'] === $PDO->query("SELECT username FROM users WHERE token = '" . addslashes($_GET['token']) . "'")->fetchColumn())
{
	if ($PDO->query("SELECT active FROM users WHERE username = '" . addslashes($_GET['username']) . "' != 'Y'")->fetchColumn())
	{
		// Set user active to Yes
		$PDO->query("UPDATE users SET active = 'Y' WHERE username = '" . addslashes($_GET['username']) . "'");
		echo "Account enabled, you can connect youself to Camagru plateform";
		$mark = 1;
		// $_SESSION['auth']['active'] = "Y";
	}
	else
		echo "Already activated.";
}
else
	echo "Token is not correct.";

$reset_2w = ob_get_clean();
if (isset($_GET['signin']) || $mark == 1)
	echo $signin;
else if (isset($_GET['signup']))
	echo $signup;
else if (isset($_GET['wrong']))
	echo $wrong;
else if (isset($_GET['reset_1']))
	echo $reset_1;
else if (isset($_GET['reset_2']))
	echo $reset_2;
else if (isset($_GET['reset_2w']))
	echo $reset_2w;
else if (isset($_GET['logout']))
{
	$_SESSION = array();
	session_destroy();
	echo $signup;
}
else
 	echo $signup;
?>
</body>
</html>
