function add_comment(id_img, heure)
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'comment.php?body=' + document.getElementById('body' + id_img).value + '&id_web_picture=' + id_img);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
	xhr.onload = function()
	{
		var all = document.createElement("DIV");
		var date = document.createElement("DIV");
		var com = document.createElement("P");
		var body = document.createTextNode(xhr.responseText);
		var time_stamp = document.createTextNode(heure);
		com.appendChild(body);
		date.appendChild(time_stamp);
		date.className += "date";
		all.appendChild(date);
		all.appendChild(com);
		all.className += "comment";
		document.getElementById("com" + id_img).appendChild(all);
	};
	xhr.send();
}
